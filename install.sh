#/bin/bash

LIBEXECDIR=${LIBEXECDIR:-/usr/local/libexec}
APTCONFDIR=${APTCONFDIR:-/etc/apt/apt.conf.d}

if ! ( [ -f 95gitlab ] && [ -f gitlab-postinstall ] && [ -f gitlab-preinstall ] ); then
  echo >&2 "This script must be run from the distribution directory"
  exit 1
fi

if ! [ -d $APTCONF_DIR ]; then
  echo >&2 "Apt configuration directory $APTCONF_DIR not found. Run as"
  echo >&2 "\$ APTCONF_DIR=your_aptconf_dir $0"
  exit 1
fi
   
SRCDIR=$(pwd)
if ( set -x; mkdir -p $LIBEXECDIR ) && \
   ( set -x; ln -s -f --target-directory=$LIBEXECDIR $SRCDIR/gitlab-preinstall ) && \
   ( set -x; ln -s -f --target-directory=$LIBEXECDIR $SRCDIR/gitlab-postinstall) && \
   ( set -x; ln -s -f --target-directory=$APTCONFDIR $SRCDIR/95gitlab ); then
  echo >&2 "Install completed successfully."
  exit 0
else
  echo >&2 "Install did not succeed because of this error."
  exit 1
fi

