Copyright (c) 2015 Smart Action Company LLC (see LICENSE.md)

This is a set of scripts that hook into `apt-get`to maintain local
changes to Omnibus GitLab EE setup.

This will probably work for GitLab CE as well, but has not been tested.
You will need to change the hardcoded package pattern in the scripts.

The script simplifies the maintenance of small local changes to your
GitLab codebase between upgrades. I am using this only for `.rb` files
so far; keep in mind that there are files, e. g. assets, that cannot
be simply replaced without precompilation (and this is tricky under
Omnibus setup).

# Initial Installation

First of all, create a Git repository in GitLab railsinstallation
directory `/opt/gitlab/embedded/service/gitlab-rails`, import all files
and commit.  If you are savvy and know what you are doing, you might
want to gitignore some files. If not, just accept all. This is important
that you do not have untracked files reported by `git status`. If you
discover later in the process a file annoingly changed by GitLab
reconfiguration, you might consider `git update-index
--assume-unchanged` or that specific file.

Next, Rename the branch as `git branch -m vendor` (the names `vendor`
and `local` are hardcoded in these scripts), tag your initial commit
(the scripts will use tags formatted as `vendor-8.0.4-ee`, you may stay
in line with that), then fork off the `git checkout -b local` branch.
Apply your local changes and reconfigure and/or restart GitLab (most
changes require only a restart).

Checkout the files from this repository somewhere (I am using
`/usr/local/share/gitlab`), `cd` to that directory and run `./install`
with root access. This should get everything set up.

Finally, freeze the GitLab  updates with `apt-mark hold gitlab-ee` so
that you upgrade only specifically with `apt-get install gitlab-ee`.
This script was not tested updating gitlab=ee with other packages.

# GitLab upgrade

Under normal conditions, the `local` branch must be current. When
upgrading with `apt-get install gitlab-ee`, the hooks do the following:
 * Do sanity checks and abort if not all conditions are met;
 * Switch to the `vendor` branch;
 * Allow `apt-get` to install the upgrade;
 * Re-freeze `gitlab-ee` to prvent automatic update;
 * Commit the upgrade onto the vendor branch;
 * Tag it appropriately;
 * Switch to the `local` branch;
 * **Initiate** a rebase all your local changes to the new vendor import.

Since the rebase may fail with a conflict, double-check the output. You
may need to resolve the conflict and `git rebase --continue`, possibly
more than once. The `rerere` option is your friend.

Final word, do not trust this (or any other of you tools). Double-check
everything you can. Good luck.

@kkm
